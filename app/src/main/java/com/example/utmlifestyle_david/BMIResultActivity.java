package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.utmlifestyle_david.Model.BMI_HISTORY;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@RequiresApi(api = Build.VERSION_CODES.O)
public class BMIResultActivity extends AppCompatActivity {
    String height, weight, bmiresult;
    double iheight, iweight, ibmiresult;
    TextView textview_result, textview_remind;
    DecimalFormat decimalFormat = new DecimalFormat("#.#");
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMMM");
    List<BMI_HISTORY> bmi_historyArrayList = new ArrayList<>();
    Random ran1 = new Random(9999);
    boolean check = true;

    DatabaseReference reference,reference2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi_result);
        textview_result = findViewById(R.id.textview_bmi_result);
        textview_remind = findViewById(R.id.textview_remind);

        Intent intent = getIntent();

        Bundle bundle = intent.getBundleExtra("data");
        height = bundle.getString("height");
        weight = bundle.getString("weight");

        iheight = Double.parseDouble(height)/100;
        iweight = Double.parseDouble(weight);

        //Toast.makeText(Bmi_result.this, "height"+ iheight + "weight"+ iweight, Toast.LENGTH_SHORT).show();

        ibmiresult = iweight/(iheight*iheight);
        //bmiresult = String.valueOf();
        textview_result.setText(decimalFormat.format(ibmiresult));

        if(ibmiresult<18.5){
            textview_remind.setText("you are too thin");
        }

        else if(ibmiresult>18.4 && ibmiresult<25){
            textview_remind.setText("you are normal");
        }

        else if(ibmiresult>24.9 && ibmiresult<30){
            textview_remind.setText("you are over weight already careful");
        }

        else if(ibmiresult>29.9 && ibmiresult<35){
            textview_remind.setText("you are obess class 1");
        }

        else if(ibmiresult>34.9 && ibmiresult<40){
            textview_remind.setText("you are obess class 2");
        }

        else if(ibmiresult>39.9){
            textview_remind.setText("you are obess class 3");
        }

        reference = FirebaseDatabase.getInstance().getReference("BMI");
        reference.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                bmi_historyArrayList.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()){
                    BMI_HISTORY bmi_history = snapshot1.getValue(BMI_HISTORY.class);
                    bmi_historyArrayList.add(bmi_history);
                    if(check){


                        check=false;
                    }
                    else{

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        LocalDateTime now = LocalDateTime.now();
        int i = ran1.nextInt();

        reference2 = FirebaseDatabase.getInstance().getReference("BMI").child("user"+ran());
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", "user"+bmi_historyArrayList.size()+1);
        hashMap.put("username", "david");
        hashMap.put("date",c2s(now.getDayOfWeek().toString())+", "+dtf.format(now));
        hashMap.put("height", height);
        hashMap.put("weight", weight);
        hashMap.put("bmi", decimalFormat.format(ibmiresult));
        //reference2.push().setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
        reference2.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                                        /*Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();*/
                    //Toast.makeText(BMIResultActivity.this, "Data store successful", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public String c2s(String text){
        char[] chars=text.toCharArray();
        for(int i = 1; i< chars.length;i++){
            chars[i]+=32;
        }

        String new_letter =new String(chars);
        return new_letter;
    }

    public String ran(){
        String s = String.valueOf(Math.random());
        String[] sc = s.split("");
        String sn = "";
        for(int i = 3; i<8;i++){
            sn +=sc[i];
        }
        //Toast.makeText(BMIResultActivity.this, sn, Toast.LENGTH_SHORT).show();
        return sn;
    }
}