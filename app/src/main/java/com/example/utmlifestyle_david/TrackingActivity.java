package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import java.text.DateFormat;
import java.util.Calendar;

public class TrackingActivity extends AppCompatActivity {

    CardView cardView;
    LinearLayout linearLayout,linearLayout3;

    int[] height = new int[1];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);
        Calendar calendar = Calendar.getInstance();
        String currentDate = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());

        final TextView textViewDate = findViewById(R.id.textview_fitness_tracker_date);
        cardView = findViewById(R.id.card);
        linearLayout = findViewById(R.id.linearLayout3);
        linearLayout3 = findViewById(R.id.linearLayout3);
        textViewDate.setText(currentDate);

        /*cardView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                height[0] = Math.round((linearLayout.getY())-(textViewDate.getHeight()+textViewDate.getY()));
                cardView.getLayoutParams().height= height[0];
                cardView.getLayoutParams().width= height[0];
                cardView.requestLayout();
                cardView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });*/

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TrackingActivity.this, TrackingMapActivity.class));
            }
        });

        linearLayout3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TrackingActivity.this, TrackingListActivity.class));
            }
        });
    }
}