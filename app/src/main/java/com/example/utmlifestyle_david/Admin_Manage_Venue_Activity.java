package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmlifestyle_david.Dialog.LoadDialogUtil;
import com.example.utmlifestyle_david.Model.Venue_List;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Admin_Manage_Venue_Activity extends AppCompatActivity {

    Button button_enter_manage;
    RecyclerView recyclerView_venue_list;

    Venue_List_Adapter venue_list_adapter;
    List<Venue_List> venue_listList = new ArrayList<>();

    LoadDialogUtil loadDialogUtil = new LoadDialogUtil();

    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_manage_venue);

        loadDialogUtil.createLoadingDialog(Admin_Manage_Venue_Activity.this, "Loading");

        button_enter_manage = findViewById(R.id.button_enter_manage);

        recyclerView_venue_list = findViewById(R.id.recyclerview_venue_list);
        recyclerView_venue_list.setHasFixedSize(true);
        recyclerView_venue_list.setLayoutManager(new LinearLayoutManager(Admin_Manage_Venue_Activity.this));

        reference  = FirebaseDatabase.getInstance().getReference("Venue_List");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                loadDialogUtil.closeDialog(loadDialogUtil.loadingDialog);
                venue_listList.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()){
                    Venue_List venue_list = snapshot1.getValue(Venue_List.class);
                    venue_listList.add(venue_list);
                }

                venue_list_adapter = new Venue_List_Adapter(Admin_Manage_Venue_Activity.this, venue_listList);
                venue_list_adapter.setOnItemClickListener(new Venue_List_Adapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Bundle bundle = new Bundle();
                        bundle.putString("name", venue_listList.get(position).getName());
                        bundle.putString("id", venue_listList.get(position).getId());
                        Intent intent = new Intent(Admin_Manage_Venue_Activity.this, Admin_Manage_Delete_Venue_Activity.class);
                        intent.putExtra("data", bundle);
                        startActivity(intent);
                    }
                });
                recyclerView_venue_list.setAdapter(venue_list_adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        button_enter_manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Admin_Manage_Venue_Activity.this, Add_Venue.class));
            }
        });
    }
}