package com.example.utmlifestyle_david;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmlifestyle_david.Model.Venue_booking_statuss;

import java.util.List;

public class Booking_List_Adapter extends RecyclerView.Adapter<Booking_List_Adapter.ViewHolder> {

    Context context;
    List<Venue_booking_statuss> venue_listList;
    private OnItemClickListener mOnItemClickListener;

    public Booking_List_Adapter(Context context, List<Venue_booking_statuss> venue_listList){
        this.context = context;
        this.venue_listList = venue_listList;
    }

    @NonNull
    @Override
    public Booking_List_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_booking_list_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Booking_List_Adapter.ViewHolder holder, final int position) {
        holder.textView_id.setText(venue_listList.get(position).getRent_id());
        holder.textView_name.setText(venue_listList.get(position).getName());
        holder.textView_date.setText(venue_listList.get(position).getDate());
        holder.textView_time.setText(venue_listList.get(position).getTime());
        if (mOnItemClickListener != null) {
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onItemClick(view, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.venue_listList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView_id, textView_name, textView_date, textView_time;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView_id = itemView.findViewById(R.id.textview_booking_id);
            textView_name = itemView.findViewById(R.id.textview_booking_name);
            textView_date = itemView.findViewById(R.id.textview_booking_date);
            textView_time = itemView.findViewById(R.id.textview_booking_time);
            cardView = itemView.findViewById(R.id.card);
        }
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener){
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
