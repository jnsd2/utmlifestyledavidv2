package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmlifestyle_david.Dialog.LoadDialogUtil;
import com.example.utmlifestyle_david.Model.Venue_booking_statuss;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Admin_Booking_Venue_Activity extends AppCompatActivity {

    Button button_new_venue_booking;

    LoadDialogUtil loadDialogUtil = new LoadDialogUtil();

    RecyclerView recyclerView_booking_list;

    Booking_List_Adapter booking_list_adapter;
    List<Venue_booking_statuss> booking_listList = new ArrayList<>();

    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_booking_venue);
        loadDialogUtil.createLoadingDialog(Admin_Booking_Venue_Activity.this,"Loading");
        button_new_venue_booking = findViewById(R.id.button_new_booking);

        recyclerView_booking_list = findViewById(R.id.recyclerview_booking_list);
        recyclerView_booking_list.setHasFixedSize(true);
        recyclerView_booking_list.setLayoutManager(new LinearLayoutManager(Admin_Booking_Venue_Activity.this));

        reference  = FirebaseDatabase.getInstance().getReference("Venue_booking_statusss");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                loadDialogUtil.closeDialog(loadDialogUtil.loadingDialog);
                booking_listList.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()){
                    Venue_booking_statuss venue_booking_statuss = snapshot1.getValue(Venue_booking_statuss.class);
                    if(venue_booking_statuss.getRent_status().equals("n")){
                        booking_listList.add(venue_booking_statuss);
                    }
                }

                booking_list_adapter = new Booking_List_Adapter(Admin_Booking_Venue_Activity.this, booking_listList);
                booking_list_adapter.setOnItemClickListener(new Booking_List_Adapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Bundle bundle = new Bundle();
                        bundle.putString("name", booking_listList.get(position).getName());
                        bundle.putString("id", booking_listList.get(position).getRent_id());
                        bundle.putString("time", booking_listList.get(position).getTime());
                        bundle.putString("date", booking_listList.get(position).getDate());
                        Intent intent = new Intent(Admin_Booking_Venue_Activity.this, Admin_Booking_Delete_Activity.class);
                        intent.putExtra("data", bundle);
                        startActivity(intent);
                    }
                });
                recyclerView_booking_list.setAdapter(booking_list_adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        button_new_venue_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Admin_Booking_Venue_Activity.this, Add_Booking_Venue_Activity.class));
            }
        });
    }
}