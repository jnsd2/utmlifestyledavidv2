package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.utmlifestyle_david.Fragment.Fragment_Add_Venue;
import com.example.utmlifestyle_david.Fragment.Fragment_Add_Venue_Success;
import com.example.utmlifestyle_david.Fragment.Fragment_Delete_Booking;
import com.example.utmlifestyle_david.Fragment.Fragment_Delete_Booking_Success;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class Admin_Booking_Delete_Activity extends AppCompatActivity {

    TextView textView;
    FrameLayout frameLayout;
    ImageView imageView;
    Button button_delete;

    DatabaseReference reference;

    String name, id, date, time;

    Fragment_Delete_Booking fragment_delete_booking;
    Fragment_Delete_Booking_Success fragment_delete_booking_success = new Fragment_Delete_Booking_Success();

    Fragment fragment = null;
    List<Fragment> fragmentList = new ArrayList<>();
    int i = 0;
    final int[] height = new int[1];

    WindowManager wm;
    Display displays;
    DisplayMetrics metrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_booking_delete);
        wm = (WindowManager) getApplicationContext().getSystemService(Admin_Booking_Delete_Activity.this.WINDOW_SERVICE);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        name = bundle.getString("name");
        id = bundle.getString("id");
        date = bundle.getString("date");
        time = bundle.getString("time");

        fragment_delete_booking = new Fragment_Delete_Booking(name, id, date ,time);

        displays = wm.getDefaultDisplay();
        metrics = new DisplayMetrics();
        displays.getMetrics(metrics);

        frameLayout = findViewById(R.id.fragment);
        imageView = findViewById(R.id.imageView_top);

        frameLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                height[0] = Math.round((metrics.heightPixels)-(imageView.getHeight()+imageView.getY()));
                frameLayout.getLayoutParams().height= height[0];
                frameLayout.requestLayout();
                frameLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });


        fragmentList.add(fragment_delete_booking);
        fragmentList.add(fragment_delete_booking_success);
        fragment = fragmentList.get(i);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment,fragment).commit();

        fragment_delete_booking.setOnButtonClick(new Fragment_Add_Venue.OnButtonClick() {
            @Override
            public void onClick(View view) {
                i++;
                fragment = fragmentList.get(i);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment,fragment).commit();
            }
        });



        /*


        textView = findViewById(R.id.textview_bookingid_delete);
        button_delete = findViewById(R.id.button_booking_delete);
        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        textView.setText(id);*/
    }

}