package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Add_Booking_Success_Activity extends AppCompatActivity {

    TextView textview_id, textview_detail;

    String rent_id, detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_booking_success);

        textview_id = findViewById(R.id.textview_id);
        textview_detail = findViewById(R.id.textview_detail);

        Intent intent = getIntent();

        Bundle bundle = intent.getBundleExtra("bundle");
        rent_id = bundle.getString("rent_id");
        detail = bundle.getString("detail");
        textview_id.setText(rent_id);
        textview_detail.setText(detail);
    }
}