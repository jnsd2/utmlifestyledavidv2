package com.example.utmlifestyle_david;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@RequiresApi(api = Build.VERSION_CODES.O)
public class TrackingMapActivity extends AppCompatActivity implements LocationListener, OnMapReadyCallback {
    //timer
    TimerTask timerTask;
    Timer timer;
    Double time = 0.0;
    int timerStarted = 0;
    int seconds;
    int minutes;
    int hours;
    double cal;
    String currentran;

    //stepcount
    private double MagnitudePrevious = 0;
    private Integer stepCount = 0;


    private static final String TAG = "TrackingMapActivity";
    private android.location.LocationManager LocationManager;//定位的管理器类
    private Location mCurrentLocation;
    private ArrayList<LatLng> points; //added
    Polyline line; //added
    double totaldistance=0;
    TextView textview_cal, textview_time, textview_distance, textview_step, textview_running;
    TextView textview_cal2, textview_time2, textview_distance2;
    ImageButton imagebutotn_play, imagebutton_stop;
    GridLayout gridLayout, gridLayout2;
    DecimalFormat newFormat = new DecimalFormat("####");

    private String title;
    double l;
    double y;
    private List<Address> addresses;
    SupportMapFragment fm;
    MarkerOptions options = new MarkerOptions();

    FirebaseAuth auth;
    DatabaseReference reference;
    String starttime;

    //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMMM");
    DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("HH:mm");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_map);
        points = new ArrayList<LatLng>(); //added

        textview_distance = findViewById(R.id.textview_distance);
        textview_cal = findViewById(R.id.textview_cal);
        textview_time = findViewById(R.id.textview_time);
        textview_step = findViewById(R.id.textview_step);
        imagebutotn_play = findViewById(R.id.imagebutton_play);
        imagebutton_stop = findViewById(R.id.imagebutton_stop);
        gridLayout = findViewById(R.id.gridlayout);
        gridLayout2 = findViewById(R.id.gridlayout_result);
        textview_cal2 = findViewById(R.id.textview_cal2);
        textview_time2 = findViewById(R.id.textview_time2);
        textview_distance2 = findViewById(R.id.textview_distance2);
        textview_running = findViewById(R.id.textView2);

        definemap();
        timer = new Timer();

        SensorManager serviceManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor sensor =  serviceManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        SensorEventListener sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if(sensorEvent!=null){
                    float x = sensorEvent.values[0];
                    float y = sensorEvent.values[1];
                    float z = sensorEvent.values[2];

                    double Magnitude = Math.sqrt(x*x+y*y+z*z);
                    double MagnitudeDelta = Magnitude - MagnitudePrevious;
                    MagnitudePrevious = Magnitude;

                    if(MagnitudeDelta > 18){
                        stepCount++;
                    }

                    textview_step.setText(stepCount.toString());
                }
                else{
                    //Toast.makeText(MainActivity.this,"not",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };
        serviceManager.registerListener(sensorEventListener,sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart fired ..............");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop fired ..............");
    }


    @Override
    public void onLocationChanged(Location location) {
        //Toast.makeText(this, "start", Toast.LENGTH_LONG).show();
        if(timerStarted==1) {
            Log.d(TAG, "Firing onLocationChanged..............................................");
            mCurrentLocation = location;

            float accuracy = location.getAccuracy();
            Log.d("iFocus", "The amount of accuracy is " + accuracy);
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            //Toast.makeText(this, "latitude:" + latitude + "||longitude:" + longitude, Toast.LENGTH_LONG).show();

            fm.getMapAsync(TrackingMapActivity.this);
        }
        else{
            //Toast.makeText(this, "else", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        options.position(currentLatLng).title(title);

        Log.d(TAG, "Marker added.............................");
        points.add(currentLatLng); //added

        if(points.size()==1){

        }
        else{
            totaldistance = totaldistance+CalculationByDistance(points.get(points.size()-2), points.get(points.size()-1));
        }

        textview_distance.setText(String. format("%.2f", totaldistance));

        googleMap.clear();  //clears all Markers and Polylines

        PolylineOptions poptions = new PolylineOptions().width(10).color(Color.RED).geodesic(true);
        for (int i = 0; i < points.size(); i++) {
            LatLng point = points.get(i);
            poptions.add(point);
        }

        line = googleMap.addPolyline(poptions); //add Polyline

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,18));
        googleMap.addMarker(options);
        Log.d(TAG, "Zoom done.............................");
    }

    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;

        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return Radius * c;
    }

    public void definemap(){
        Log.d(TAG, "onCreate ...............................");


        fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map);
        LocationManager = (android.location.LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean isopen = LocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!isopen) {
            //进入gps的设置页面
            Intent intent = new Intent();
            intent.setAction(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivityForResult(intent, 0);
        }
        if (ActivityCompat.checkSelfPermission(TrackingMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        LocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, TrackingMapActivity.this);
    }

    public void resetTapped(View view){
        AlertDialog.Builder resetAlert = new AlertDialog.Builder(this);
        resetAlert.setTitle("End Tracking");
        resetAlert.setMessage("are you sure you want to stop tracking?");
        resetAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                if(timerTask != null){
                    timerTask.cancel();
                    setButtonUI(R.drawable.button_play);

                    textview_cal2.setText(String. format("%.2f",countcal(70,hours,minutes)));
                    textview_time2.setText(formatTime(seconds, minutes, hours));
                    textview_distance2.setText(String. format("%.2f",totaldistance));

                    time = 0.0;
                    cal = 0.0;
                    stepCount = 0;
                    timerStarted = 3;
                    //textview_time.setText(formatTime(0,0,0));
                    LocalDateTime now = LocalDateTime.now();
                    textview_running.setVisibility(View.GONE);
                    imagebutton_stop.setVisibility(View.GONE);
                    imagebutotn_play.setBackgroundResource(R.drawable.button_home);
                    textview_time.setText(dtf.format(now)+", "+starttime+" - "+dtf2.format(now));
                    gridLayout.setVisibility(View.GONE);
                    gridLayout2.setVisibility(View.VISIBLE);


                    //System.out.println(dtf.format(now));


                    currentran = ran();
                    reference = FirebaseDatabase.getInstance().getReference("Map").child("user"+currentran);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("id", "1");
                    hashMap.put("username", "david");
                    hashMap.put("time", formatTime(seconds,  minutes, hours));
                    hashMap.put("distance", String. format("%.2f",totaldistance));
                    hashMap.put("timing", "morning");
                    hashMap.put("cal_burn", String. format("%.2f",countcal(70,hours,minutes)));
                    hashMap.put("date", dtf.format(now));
                    hashMap.put("start_time", starttime);
                    hashMap.put("end_time", dtf2.format(now));

                    reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                        /*Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();*/
                                //Toast.makeText(TrackingMapActivity.this, "Data store successful", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    HashMap<String, String> hashMap2 = new HashMap();
                    for (int ii = 0; ii < points.size(); ii++) {
                        reference = FirebaseDatabase.getInstance().getReference("Map").child("user"+currentran).child("point").child("point"+String.format("%04d",ii));

                        LatLng currentLatLng = points.get(ii);
                        hashMap2.put("lag",String.valueOf(currentLatLng.latitude));
                        hashMap2.put("lon",String.valueOf(currentLatLng.longitude));
                        reference.setValue(hashMap2).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                        /*Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();*/
                                    //Toast.makeText(TrackingMapActivity.this, "Point store successful", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                    /*for (int ii = 0; ii < points.size(); ii++) {
                        reference = FirebaseDatabase.getInstance().getReference("Map").child("user1").child("point").child("lat"+ii);
                        LatLng currentLatLng = points.get(ii);
                        hashMap2.put("lag",String.valueOf(currentLatLng.latitude));
                        reference.setValue(hashMap2).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                        Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    //Toast.makeText(TrackingMapActivity.this, "Point store successful", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }


                    HashMap<String, String> hashMap3 = new HashMap();
                    reference = FirebaseDatabase.getInstance().getReference("Map").child("user1").child("point").child("log");
                    for (int ii = 0; ii < points.size(); ii++) {
                        LatLng currentLatLng = points.get(ii);
                        hashMap3.put("lon",String.valueOf(currentLatLng.longitude));
                    }
                    reference.setValue(hashMap3).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                        Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                //Toast.makeText(TrackingMapActivity.this, "Point store successful", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });*/
                }
            }
        });

        resetAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){

            }
        });

        resetAlert.show();
    }

    public void startStopTapped(View view){
        if(timerStarted == 0){
            timerStarted = 1;
            setButtonUI(R.drawable.button_pause);
            LocalDateTime now = LocalDateTime.now();
            starttime = dtf2.format(now);
            startTimer();
        }
        else if(timerStarted == 1){
            timerStarted = 0;
            setButtonUI(R.drawable.button_play);

            timerTask.cancel();
        }
        else{
            finish();
        }
    }

    private void setButtonUI(int drawable){
        imagebutotn_play.setBackgroundResource(drawable);
        if((drawable==R.drawable.button_pause)||(seconds!=0)){
            imagebutton_stop.setVisibility(View.VISIBLE);
        }
        else{
            imagebutton_stop.setVisibility(View.GONE);
        }
    }

    private void startTimer(){
        //Toast.makeText(MainActivity.this,"ha",Toast.LENGTH_LONG).show();
        timerTask = new TimerTask(){
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        time++;
                        textview_time.setText(getTimerText());
                    }
                });

            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 1000);
    }

    private String getTimerText(){
        int rounded = (int) Math.round(time);
        imagebutton_stop.setVisibility(View.VISIBLE);
        seconds = ((rounded % 86400) % 3000) % 60;
        minutes = ((rounded % 86400) % 3000) / 60;
        hours = (rounded % 86400) / 3000;
        textview_cal.setText(String. format("%.2f",countcal(70,hours,minutes)));
        return formatTime(seconds, minutes, hours);
    }

    private String formatTime(int seconds, int minutes, int hours){
        return String.format("%02d", hours)+":"+String.format("%02d",minutes)+ ":"+String.format("%02d",seconds);
    }

    private double countcal(double weight, int hour, int minutes){
        double hours = (double) hour + (double) minutes/60;
        cal = weight * hours * 1.036;

        return cal;
    }

    public String ran(){
        String s = String.valueOf(Math.random());
        String[] sc = s.split("");
        String sn = "";
        for(int i = 3; i<8;i++){
            sn +=sc[i];
        }
        //Toast.makeText(TrackingMapActivity.this, sn, Toast.LENGTH_SHORT).show();
        return sn;
    }
}