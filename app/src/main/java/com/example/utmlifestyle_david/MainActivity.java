package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    Button button_bmi, button_tracking, button_venue, button_booking, button_schedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_bmi = findViewById(R.id.button_bmi);
        button_tracking = findViewById(R.id.button_tracking);
        button_venue = findViewById(R.id.button_venue);
        button_booking = findViewById(R.id.button_booking);
        button_schedule = findViewById(R.id.button_schedule);

        button_bmi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,BMIActivity.class));
            }
        });

        button_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,TrackingActivity.class));
            }
        });

        button_venue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Admin_Manage_Venue_Activity.class));
            }
        });

        button_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Admin_Schedule_Venue_Activity.class));
            }
        });

        button_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Admin_Booking_Venue_Activity.class));
            }
        });
    }
}