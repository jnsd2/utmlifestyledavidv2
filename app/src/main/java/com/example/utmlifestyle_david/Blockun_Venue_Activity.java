package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.utmlifestyle_david.Fragment.Fragment_Add_Venue;
import com.example.utmlifestyle_david.Fragment.Fragment_Date_Block;
import com.example.utmlifestyle_david.Fragment.Fragment_Date_Block_Success;
import com.example.utmlifestyle_david.Fragment.Fragment_Date_Unblock;
import com.example.utmlifestyle_david.Fragment.Fragment_Date_Unblock_Success;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class Blockun_Venue_Activity extends AppCompatActivity {

    Button button_delete;
    ImageView imageView;
    FrameLayout frameLayout;

    String name, id, date, time;

    Fragment_Date_Unblock fragment_date_unblock;
    Fragment_Date_Unblock_Success fragment_date_unblock_success = new  Fragment_Date_Unblock_Success();

    Fragment fragment = null;
    List<Fragment> fragmentList = new ArrayList<>();
    int i = 0;
    final int[] height = new int[1];

    WindowManager wm;
    Display displays;
    DisplayMetrics metrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blockun_venue);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        name = bundle.getString("name");
        id = bundle.getString("id");
        date = bundle.getString("date");
        time = bundle.getString("time");

        fragment_date_unblock = new Fragment_Date_Unblock(id, name, date, time);

        frameLayout = findViewById(R.id.fragment);
        imageView = findViewById(R.id.imageView_top);

        wm = (WindowManager) getApplicationContext().getSystemService(Blockun_Venue_Activity.this.WINDOW_SERVICE);
        displays = wm.getDefaultDisplay();
        metrics = new DisplayMetrics();
        displays.getMetrics(metrics);

        frameLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                height[0] = Math.round((metrics.heightPixels)-(imageView.getHeight()+imageView.getY()));
                frameLayout.getLayoutParams().height= height[0];
                frameLayout.requestLayout();
                frameLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        fragmentList.add(fragment_date_unblock);
        fragmentList.add(fragment_date_unblock_success);
        fragment = fragmentList.get(i);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment,fragment).commit();

        fragment_date_unblock.setOnButtonClick(new Fragment_Add_Venue.OnButtonClick() {
            @Override
            public void onClick(View view) {
                fragment = fragmentList.get(i+1);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment,fragment).commit();
            }
        });

        /*



        button_delete = findViewById(R.id.button_unblock);
        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statuss").child(name+";"+date+";"+l2s(time)).child("rent_status").setValue("a");
                finish();
            }
        });*/

    }


}