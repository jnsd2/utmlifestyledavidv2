package com.example.utmlifestyle_david;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.utmlifestyle_david.Model.Venue_List;
import com.example.utmlifestyle_david.Model.Venue_booking_statuss;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Schedule_Available_Booking_Activity extends AppCompatActivity {

    Spinner spinner_schedule_sport_hall;
    DatePicker datePicker;
    Button button_schedualte_comfirm;

    DatabaseReference reference, reference2;

    List<Venue_List> venueListList = new ArrayList<>();
    List<Venue_booking_statuss> venue_booking_statusList = new ArrayList<>();
    List<String> sport_hall = new ArrayList<>();
    List<String> avaiable_date = new ArrayList<>();
    List<String> avaiable_time = new ArrayList<>();

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapterdate;

    String month;
    String text_spinnerplace, text_Selecteddate="";
    Venue_booking_statuss venue_booking_statuss1 = new Venue_booking_statuss();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_available_booking);

        spinner_schedule_sport_hall = findViewById(R.id.spinner_schedule_sport_hall);
        button_schedualte_comfirm = findViewById(R.id.button_schedule_confirm);
        button_schedualte_comfirm.setEnabled(false);
        datePicker = findViewById(R.id.datepicker_schedule_date);

        reference = FirebaseDatabase.getInstance().getReference().child("Venue_List");
        reference2 = FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss");

        reference2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                venue_booking_statusList.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()){
                    Venue_booking_statuss venue_booking_statuss = snapshot1.getValue(Venue_booking_statuss.class);
                    venue_booking_statusList.add(venue_booking_statuss);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                sport_hall.clear();
                venueListList.clear();
                for (DataSnapshot snapshot1 : snapshot.getChildren()) {
                    Venue_List venue_list = snapshot1.getValue(Venue_List.class);
                    venueListList.add(venue_list);
                    sport_hall.add(venue_list.getName());
                }
                sport_hall = removeDuplicateWithOrder(sport_hall);
                /*if(sport_hall.size()==0){
                    sport_hall.add(venueListList.get(0).getName());
                }

                for(int j = 0; j<venueListList.size();j++) {
                    for(int i = 0 ; i<sport_hall.size();i++) {
                        if (venueListList.get(j).getName().equals(sport_hall.get(i))) {

                        } else {
                            sport_hall.add(venueListList.get(j).getName());
                        }
                    }
                }*/

                adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, sport_hall);
                adapter.setDropDownViewResource(R.layout.spinner_text);
                spinner_schedule_sport_hall.setAdapter(adapter);
                spinner_schedule_sport_hall.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        text_spinnerplace = parent.getItemAtPosition(position).toString();

                        if(!text_Selecteddate.equals("")){
                            //Toast.makeText(Schedule_Available_Booking_Activity.this, venue_booking_statusList.size()+"",Toast.LENGTH_SHORT).show();
                            if(venue_booking_statusList.size()==0){
                                button_schedualte_comfirm.setEnabled(true);
                            }
                            for(int j = 0 ; j < venue_booking_statusList.size(); j++){
                                //Toast.makeText(Schedule_Available_Booking_Activity.this, i+"/"+(i1+1)+"/"+i2,Toast.LENGTH_SHORT).show();
                                //Toast.makeText(Schedule_Available_Booking_Activity.this, venue_booking_statusList.get(j)+":"+text_spinnerplace+"\n"+venue_booking_statusList.get(j).getDate()+":"+text_Selecteddate,Toast.LENGTH_SHORT).show();
                                if(venue_booking_statusList.get(j).getName().equals(text_spinnerplace)&&venue_booking_statusList.get(j).getDate().equals(text_Selecteddate)){
                                    //Toast.makeText(Schedule_Available_Booking_Activity.this, "yo",Toast.LENGTH_SHORT).show();
                                    button_schedualte_comfirm.setEnabled(false);
                                    break;
                                }
                                else{
                                    //Toast.makeText(Schedule_Available_Booking_Activity.this, "bo",Toast.LENGTH_SHORT).show();
                                    button_schedualte_comfirm.setEnabled(true);

                                }
                            }
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onCancelled (@NonNull DatabaseError error){

            }
        });

        datePicker.setMinDate(System.currentTimeMillis());

        datePicker.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                //Toast.makeText(Schedule_Available_Booking_Activity.this, i+"/"+(i1+1)+"/"+i2,Toast.LENGTH_SHORT).show();

                switch (i1+1){
                    case 1:
                        month = "Jan";
                        break;
                    case 2:
                        month = "Feb";
                        break;
                    case 3:
                        month = "May";
                        break;
                    case 4:
                        month = "Apr";
                        break;
                    case 5:
                        month = "Mar";
                        break;
                    case 6:
                        month = "Jun";
                        break;
                    case 7:
                        month = "Jul";
                        break;
                    case 8:
                        month = "Oct";
                        break;
                    case 9:
                        month = "Sep";
                        break;
                    case 10:
                        month = "Oct";
                        break;
                    case 11:
                        month = "Non";
                        break;
                    case 12:
                        month = "Dis";
                        break;
                    default:
                        break;
                }




                text_Selecteddate = i2+" "+month+" "+i;
                /*if (!text_Selecteddate.equals("")){
                    Toast.makeText(Schedule_Available_Booking_Activity.this, "haha",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(Schedule_Available_Booking_Activity.this, "hoho",Toast.LENGTH_SHORT).show();
                }*/

                if(!text_Selecteddate.equals("")){
                    //Toast.makeText(Schedule_Available_Booking_Activity.this, venue_booking_statusList.size()+"",Toast.LENGTH_SHORT).show();
                    if(venue_booking_statusList.size()==0){
                        button_schedualte_comfirm.setEnabled(true);
                    }
                    else{
                        for(int j = 0 ; j < venue_booking_statusList.size(); j++){
                            //Toast.makeText(Schedule_Available_Booking_Activity.this, i+"/"+(i1+1)+"/"+i2,Toast.LENGTH_SHORT).show();
                            //Toast.makeText(Schedule_Available_Booking_Activity.this, venue_booking_statusList.get(j)+":"+text_spinnerplace+"\n"+venue_booking_statusList.get(j).getDate()+":"+text_Selecteddate,Toast.LENGTH_SHORT).show();
                            if(venue_booking_statusList.get(j).getName().equals(text_spinnerplace)&&venue_booking_statusList.get(j).getDate().equals(text_Selecteddate)){
                                //Toast.makeText(Schedule_Available_Booking_Activity.this, "yo",Toast.LENGTH_SHORT).show();
                                button_schedualte_comfirm.setEnabled(false);
                                break;
                            }
                            else{
                                //Toast.makeText(Schedule_Available_Booking_Activity.this, "bo",Toast.LENGTH_SHORT).show();
                                button_schedualte_comfirm.setEnabled(true);

                            }
                        }
                    }

                }
            }
        });

        button_schedualte_comfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(text_spinnerplace==null){
                    Toast.makeText(Schedule_Available_Booking_Activity.this, "Create a venue", Toast.LENGTH_SHORT).show();
                }
                else{
                    for(int i = 5; i < 10; i++){
                        for(int j = 0; j < 24; j++){
                            reference = FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(text_spinnerplace+";"+text_Selecteddate+";"+String.format("%02d",j));
                            venue_booking_statuss1.setName(text_spinnerplace);
                            venue_booking_statuss1.setDate(text_Selecteddate);
                            venue_booking_statuss1.setTime(String.format("%02d",j)+":00 - "+String.format("%02d",j+1)+":00");
                            venue_booking_statuss1.setRent_status("a");
                            venue_booking_statuss1.setRent_person_name("");
                            venue_booking_statuss1.setRent_id("");
                            venue_booking_statuss1.setPay_method("");
                            venue_booking_statuss1.setVenue_detail("detail");
                            reference.setValue(venue_booking_statuss1).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        //Toast.makeText(venue_booking_form.this, "Done", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    }
                }

            }
        });
        //Toast.makeText(Schedule_Available_Booking_Activity.this, text_Selecteddate,Toast.LENGTH_SHORT).show();


        /**/
    }

    public static List removeDuplicateWithOrder(List list) {

        Set set = new HashSet();

        List newList = new ArrayList();

        for (Iterator iter = list.iterator(); iter.hasNext();) {

            Object element = iter.next();

            if (set.add(element))

                newList.add(element);

        }

        list.clear();

        list.addAll(newList);

        return list;

    }

    public static List removeSportHall(List list) {

        Set set = new HashSet();

        List newList = new ArrayList();

        for (Iterator iter = list.iterator(); iter.hasNext();) {

            Object element = iter.next();

            if (set.add(element))

                newList.add(element);

        }

        list.clear();

        list.addAll(newList);

        return list;

    }
}