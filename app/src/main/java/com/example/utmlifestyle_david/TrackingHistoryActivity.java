package com.example.utmlifestyle_david;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.utmlifestyle_david.Model.Point;
import com.example.utmlifestyle_david.Model.Tracking_HISTORY;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TrackingHistoryActivity extends AppCompatActivity implements OnMapReadyCallback {

    String position;
    DatabaseReference reference, reference2;
    List<LatLng> latLngs = new ArrayList<>();
    Point point;
    ArrayList<Point> points = new ArrayList<>();
    MarkerOptions options = new MarkerOptions();
    SupportMapFragment fm;
    List<Tracking_HISTORY> tracking_historyArrayList = new ArrayList<>();
    String title;
    Polyline line; //added

    TextView textview_date, textview_distance, textview_cal, textview_time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_history);
        fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map2);
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        position = bundle.getString("position");

        textview_date = findViewById(R.id.textview_date2);
        textview_distance = findViewById(R.id.textview_distance2);
        textview_cal = findViewById(R.id.textview_cal2);
        textview_time = findViewById(R.id.textview_time2);

        //Toast.makeText(TrackingHistoryActivity.this, position,Toast.LENGTH_SHORT).show();

        reference = FirebaseDatabase.getInstance().getReference("Map").child("user1").child("point");
        reference.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                points.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()){
                    point = snapshot1.getValue(Point.class);
                    points.add(point);
                }
                Toast.makeText(TrackingHistoryActivity.this,String.valueOf(points.size()),Toast.LENGTH_SHORT).show();

                fm.getMapAsync(TrackingHistoryActivity.this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        reference2 = FirebaseDatabase.getInstance().getReference("Map");
        reference2.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot snapshot1 : snapshot.getChildren()) {
                    Tracking_HISTORY tracking_history = snapshot1.getValue(Tracking_HISTORY.class);
                    tracking_historyArrayList.add(tracking_history);
                }
                textview_date.setText(tracking_historyArrayList.get(Integer.parseInt(position)).getDate()+", "+tracking_historyArrayList.get(0).getStart_time()+" - "+tracking_historyArrayList.get(0).getEnd_time());
                textview_distance.setText(tracking_historyArrayList.get(Integer.parseInt(position)).getDistance());
                textview_cal.setText(tracking_historyArrayList.get(Integer.parseInt(position)).getCal_burn());
                textview_time.setText(tracking_historyArrayList.get(Integer.parseInt(position)).getTime());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        for(int i = 0 ; i<points.size(); i++){
            LatLng currentLatLng = new LatLng(Double.parseDouble(points.get(i).getLag()), Double.parseDouble(points.get(i).getLon()));
            options.position(currentLatLng).title(title);

            latLngs.add(currentLatLng); //added
        }


        if(points.size()==1){

        }
        else{
            //totaldistance = totaldistance+CalculationByDistance(points.get(points.size()-2), points.get(points.size()-1));
        }

        //textview_distance.setText(String. format("%.2f", totaldistance));

        googleMap.clear();  //clears all Markers and Polylines

        PolylineOptions poptions = new PolylineOptions().width(10).color(Color.RED).geodesic(true);
        for (int i = 0; i < points.size(); i++) {
            LatLng point = latLngs.get(i);
            poptions.add(point);
        }

        line = googleMap.addPolyline(poptions); //add Polyline

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLngs.get(latLngs.size()-1)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngs.get(latLngs.size()-1),18));
        googleMap.addMarker(options);
    }
}