package com.example.utmlifestyle_david.Model;

public class Venue_booking_statuss {
    String name;
    String date;
    String time;
    String rent_status;
    String rent_person_name;
    String rent_id;
    String pay_method;
    String venue_detail;

    public Venue_booking_statuss(){}

    public Venue_booking_statuss(String name, String date, String time, String rent_status, String rent_person_name, String rent_id, String pay_method, String venue_detail) {
        this.name = name;
        this.date = date;
        this.time = time;
        this.rent_status = rent_status;
        this.rent_person_name = rent_person_name;
        this.rent_id = rent_id;
        this.pay_method = pay_method;
        this.venue_detail = venue_detail;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getRent_status() {
        return rent_status;
    }

    public String getRent_id() {
        return rent_id;
    }

    public String getPay_method() {
        return pay_method;
    }

    public String getVenue_detail() {
        return venue_detail;
    }

    public String getRent_person_name() {
        return rent_person_name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setRent_status(String rent_status) {
        this.rent_status = rent_status;
    }

    public void setRent_id(String rent_id) {
        this.rent_id = rent_id;
    }

    public void setPay_method(String pay_method) {
        this.pay_method = pay_method;
    }

    public void setVenue_detail(String venue_detail) {
        this.venue_detail = venue_detail;
    }

    public void setRent_person_name(String rent_person_name) {
        this.rent_person_name = rent_person_name;
    }
}
