package com.example.utmlifestyle_david.Model;

public class BMI_HISTORY {
    String id;
    String username;
    String date;
    String bmi;
    String height;
    String weight;

    BMI_HISTORY(){

    }

    public BMI_HISTORY(String id, String username, String date, String bmi, String height, String weight){
        this.id = id;
        this.username = username;
        this.date = date;
        this.bmi = bmi;
        this.height = height;
        this.weight = weight;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBmi() {
        return bmi;
    }

    public void setBmi(String bmi) {
        this.bmi = bmi;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
