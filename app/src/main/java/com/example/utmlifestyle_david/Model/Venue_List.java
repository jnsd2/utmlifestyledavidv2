package com.example.utmlifestyle_david.Model;

public class Venue_List {
    String id;
    String name;

    public Venue_List(){}

    Venue_List(String id, String name){
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
