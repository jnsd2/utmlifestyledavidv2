package com.example.utmlifestyle_david.Model;

public class Tracking_HISTORY {

    String id;
    String username;
    String date;
    String start_time;
    String end_time;
    String distance;
    String timing;
    String time;
    String cal_burn;

    Tracking_HISTORY(){

    }

    public Tracking_HISTORY(String id, String username, String date, String start_time, String end_time, String distance, String timing, String time, String cal_burn){
        this.id = id;
        this.date = date;
        this.username = username;
        this.start_time = start_time;
        this.end_time = end_time;
        this.distance = distance;
        this.timing = timing;
        this.time = time;
        this.cal_burn = cal_burn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCal_burn() {
        return cal_burn;
    }

    public void setCal_burn(String cal_burn) {
        this.cal_burn = cal_burn;
    }
}
