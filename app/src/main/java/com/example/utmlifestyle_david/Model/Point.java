package com.example.utmlifestyle_david.Model;

public class Point {
    String lag;
    String lon;

    Point(){

    }

    public Point(String lag, String lon){
        this.lag = lag;
        this.lon = lon;
    }

    public String getLag() {
        return lag;
    }

    public String getLon() {
        return lon;
    }
}
