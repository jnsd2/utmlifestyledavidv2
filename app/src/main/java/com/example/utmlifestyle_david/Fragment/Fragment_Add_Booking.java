package com.example.utmlifestyle_david.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.utmlifestyle_david.Dialog.LoadDialogUtil;
import com.example.utmlifestyle_david.Model.Venue_booking_statuss;
import com.example.utmlifestyle_david.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Fragment_Add_Booking extends Fragment {

    private Spinner spinner_sport_hall, spinner_date, spinner_time;
    private Button button_confirm;
    private LoadDialogUtil loadDialogUtil = new LoadDialogUtil();
    private DatabaseReference reference;

    private List<Venue_booking_statuss> venue_booking_statusList = new ArrayList<>();
    private List<String> sport_hall = new ArrayList<>();
    private List<String> avaiable_date = new ArrayList<>();
    private List<String> avaiable_time = new ArrayList<>();

    private ArrayAdapter<String> adapter;
    private ArrayAdapter<String> adapterdate;
    private ArrayAdapter<String> adaptertime;

    private String text_spinnerplace, text_spinnerdate, text_spinnertime;

    private Button btn;
    private OnButtonClick onButtonClick;
    private OnDataListener onDataListener;

    private Context mContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_booking, container, false);

        loadDialogUtil.createLoadingDialog(mContext, "Loading");
        spinner_sport_hall = view.findViewById(R.id.spinner_sport_hall);
        spinner_date = view.findViewById(R.id.spinner_date);
        spinner_time = view.findViewById(R.id.spinner_time);
        button_confirm = view.findViewById(R.id.button_confirm);

        reference = FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                loadDialogUtil.closeDialog(loadDialogUtil.loadingDialog);
                sport_hall.clear();
                venue_booking_statusList.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()) {
                    Venue_booking_statuss venue_booking_status1 = snapshot1.getValue(Venue_booking_statuss.class);
                    venue_booking_statusList.add(venue_booking_status1);
                    sport_hall.add(venue_booking_status1.getName());

                    /*(sport_hall.size()==0){
                        sport_hall.add(venue_booking_status1.getName());
                    }
                    else{
                        for(int i = 0; i<sport_hall.size()-1;i++){
                            if(venue_booking_statusList.get(i).getName().equals(venue_booking_status1.getName())){

                            }
                            else{
                                sport_hall.add(venue_booking_status1.getName());
                            }
                        }
                    }*/
                }
                sport_hall = removeDuplicateWithOrder(sport_hall);
                adapter = new ArrayAdapter<String>(mContext, R.layout.spinner_text, sport_hall);
                adapter.setDropDownViewResource(R.layout.spinner_text);
                spinner_sport_hall.setAdapter(adapter);
                spinner_sport_hall.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        text_spinnerplace = parent.getItemAtPosition(position).toString();
                        avaiable_date.clear();
                        for(int i = 0 ; i<venue_booking_statusList.size() ; i++){
                                if(venue_booking_statusList.get(i).getName().equals(text_spinnerplace)){
                                    avaiable_date.add(venue_booking_statusList.get(i).getDate());
                                }
                        }
                        avaiable_date = removeDuplicateWithOrder(avaiable_date);
                        /*for(int i = 0 ; i<venue_booking_statusList.size() ; i++){
                            if(avaiable_date.size()==0){
                                if(venue_booking_statusList.get(i).getName().equals(text_spinnerplace)){
                                    avaiable_date.add(venue_booking_statusList.get(i).getDate());
                                }

                            }
                            else{

                                if(venue_booking_statusList.get(i).getDate().equals(avaiable_date.get(avaiable_date.size()-1))){

                                }
                                else{
                                    avaiable_date.add(venue_booking_statusList.get(i).getDate());
                                }
                            }
                        }*/
                        adapterdate = new ArrayAdapter<String>(mContext, R.layout.spinner_text, avaiable_date);
                        adapterdate.setDropDownViewResource(R.layout.spinner_text);
                        spinner_date.setAdapter(adapterdate);
                        spinner_date.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                text_spinnerdate = parent.getItemAtPosition(position).toString();
                                //Toast.makeText(parent.getContext(),text,Toast.LENGTH_SHORT).show();
                                avaiable_time.clear();
                                for(int i = 0 ; i<venue_booking_statusList.size() ; i++){
                                    if(venue_booking_statusList.get(i).getName().equals(text_spinnerplace)&&venue_booking_statusList.get(i).getDate().equals(text_spinnerdate)&&venue_booking_statusList.get(i).getRent_status().equals("a")){
                                        avaiable_time.add(venue_booking_statusList.get(i).getTime());
                                    }
                                }

                                adaptertime = new ArrayAdapter<String>(mContext, R.layout.spinner_text, avaiable_time);
                                adaptertime.setDropDownViewResource(R.layout.spinner_text);
                                spinner_time.setAdapter(adaptertime);
                                spinner_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        text_spinnertime = parent.getItemAtPosition(position).toString();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        btn = (Button)view.findViewById(R.id.button_confirm);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(text_spinnerplace==null||text_spinnerplace==""){
                    Toast.makeText(mContext, "Create the available venue then available the date for booking",Toast.LENGTH_LONG).show();
                }
                else{
                    String[] timesplit = text_spinnertime.split("");
                    String timeshort = timesplit[0]+timesplit[1];
                    String child = text_spinnerplace+";"+text_spinnerdate+";"+timeshort;
                    //Toast.makeText(venue_booking_form.this,child,Toast.LENGTH_SHORT).show();
                    FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(child).child("pay_method").setValue("Chat");
                    FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(child).child("rent_id").setValue("A-123CH");
                    FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(child).child("rent_person_name").setValue("david");
                    FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(child).child("rent_status").setValue("n");

                    String detail = "";
                    for(int i = 0 ; i < venue_booking_statusList.size(); i++){
                        if(venue_booking_statusList.get(i).getName().equals(text_spinnerplace)&&venue_booking_statusList.get(i).getDate().equals(text_spinnerdate)&&venue_booking_statusList.get(i).getTime().equals(text_spinnertime)){
                            detail = venue_booking_statusList.get(i).getVenue_detail();
                            break;
                        }
                    }

                    Bundle bundle = new Bundle();
                    bundle.putString("rent_id", "A-123CH");
                    bundle.putString("detail",detail);
                    if(onButtonClick!=null){
                        onButtonClick.onClick(btn);
                    }
                    if(onDataListener!=null){
                        onDataListener.onListener(bundle);
                    }
                }

            }
        });
        return view;
    }

    public OnButtonClick getOnButtonClick() {
        return onButtonClick;
    }

    public void setOnButtonClick(OnButtonClick onButtonClick) {
        this.onButtonClick = onButtonClick;
    }

    public interface OnButtonClick{
        public void onClick(View view);
    }

    public interface OnDataListener{
        void onListener(Bundle bundle);
    }

    public void setOnDataListener(OnDataListener onDataListener){
        this.onDataListener = onDataListener;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public static List removeDuplicateWithOrder(List list) {

        Set set = new HashSet();

        List newList = new ArrayList();

        for (Iterator iter = list.iterator(); iter.hasNext();) {

            Object element = iter.next();

            if (set.add(element))

                newList.add(element);

        }

        list.clear();

        list.addAll(newList);

        return list;

    }
}
