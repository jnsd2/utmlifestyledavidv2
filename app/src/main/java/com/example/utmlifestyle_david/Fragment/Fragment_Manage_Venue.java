package com.example.utmlifestyle_david.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.utmlifestyle_david.Admin_Manage_Delete_Venue_Activity;
import com.example.utmlifestyle_david.Model.Venue_List;
import com.example.utmlifestyle_david.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Fragment_Manage_Venue extends Fragment {

    private Button btn,btn2;
    private TextView textView;
    private EditText editText;
    private OnButtonClick onButtonClick;
    private OnButtonClick onButtonClick2;

    private DatabaseReference reference;
    private Context mContext;

    private String id, name;

    public Fragment_Manage_Venue(String id, String name){
        this.id = id;
        this.name = name;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_venue, container, false);
        textView = view.findViewById(R.id.textview_venu_eid_delete);
        editText = view.findViewById(R.id.edittext_venue_name);
        textView.setText(id);
        editText.setText(name);
        btn = (Button)view.findViewById(R.id.button_venue_update);
        btn2 = (Button)view.findViewById(R.id.button_venue_delete);

        reference =  FirebaseDatabase.getInstance().getReference().child("Venue_List").child(name);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText.getText().toString().equals(name)){
                    Toast.makeText(mContext, "You have not make any edit", Toast.LENGTH_SHORT).show();
                }
                else if(editText.getText().toString()==null||editText.getText().toString().trim().isEmpty()){
                    Toast.makeText(mContext, "Give a name", Toast.LENGTH_SHORT).show();
                }
                else{
                    Venue_List venue_list = new Venue_List();
                    venue_list.setId(id);
                    venue_list.setName(editText.getText().toString());

                    reference = FirebaseDatabase.getInstance().getReference().child("Venue_List").child(name);
                    reference.removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            //Toast.makeText(Admin_Manage_Delete_Venue_Activity.this, "success", Toast.LENGTH_SHORT).show();
                        }
                    });

                    reference =  FirebaseDatabase.getInstance().getReference().child("Venue_List").child(editText.getText().toString());
                    reference.setValue(venue_list).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            if(onButtonClick!=null){
                                onButtonClick.onClick(btn);
                            }
                        }
                    });
                }
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reference.removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if(onButtonClick2!=null){
                            onButtonClick2.onClick(btn);
                        }
                    }
                });
            }
        });
        return view;
    }



    public OnButtonClick getOnButtonClick() {
        return onButtonClick;
    }

    public void setOnButtonClick(OnButtonClick onButtonClick) {
        this.onButtonClick = onButtonClick;
    }

    public void setOnButtonClick2(OnButtonClick onButtonClick2) {
        this.onButtonClick2 = onButtonClick2;
    }

    public interface OnButtonClick{
        public void onClick(View view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
