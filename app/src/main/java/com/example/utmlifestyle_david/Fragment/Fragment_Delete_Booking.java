package com.example.utmlifestyle_david.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.utmlifestyle_david.R;
import com.google.firebase.database.FirebaseDatabase;

public class Fragment_Delete_Booking extends Fragment {

    private Button btn;
    private Fragment_Add_Venue.OnButtonClick onButtonClick;
    private TextView textView;

    private String name, id, date, time;

    public Fragment_Delete_Booking(String name, String id, String date, String time){
        this.name = name;
        this.id = id;
        this.date = date;
        this.time = time;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_delete_booking, container, false);
        btn = (Button)view.findViewById(R.id.button_booking_delete);
        textView = view.findViewById(R.id.textview_booking_id_delete);
        textView.setText(id);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(name+";"+date+";"+l2s(time)).child("pay_method").setValue("");
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(name+";"+date+";"+l2s(time)).child("rent_id").setValue("");
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(name+";"+date+";"+l2s(time)).child("rent_person_name").setValue("");
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(name+";"+date+";"+l2s(time)).child("rent_status").setValue("a");
                if(onButtonClick!=null){
                    onButtonClick.onClick(btn);
                }
            }
        });
        return view;
    }

    public Fragment_Add_Venue.OnButtonClick getOnButtonClick() {
        return onButtonClick;
    }

    public void setOnButtonClick(Fragment_Add_Venue.OnButtonClick onButtonClick) {
        this.onButtonClick = onButtonClick;
    }

    public interface OnButtonClick{
        public void onClick(View view);
    }

    String l2s(String time){
        String[] sc = time.split("");
        String sn = "";
        for(int i = 0; i<2;i++){
            sn +=sc[i];
        }
        //Toast.makeText(TrackingMapActivity.this, sn, Toast.LENGTH_SHORT).show();
        return sn;
    }
}
