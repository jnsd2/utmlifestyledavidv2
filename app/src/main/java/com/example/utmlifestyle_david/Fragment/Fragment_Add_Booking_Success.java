package com.example.utmlifestyle_david.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.utmlifestyle_david.R;

public class Fragment_Add_Booking_Success extends Fragment {
    private Button btn;
    private Fragment_Add_Booking.OnDataListener onDataListener;
    private Bundle bundle;
    TextView textview_id, textview_detail;
    String rent_id, detail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_add_booking_successful, container, false);
        btn = (Button)view.findViewById(R.id.button_add_venue_successful);
        textview_id = view.findViewById(R.id.textview_add_booking_id);
        textview_detail = view.findViewById(R.id.textview_add_booking_detail);
        rent_id = this.bundle.getString("rent_id");
        detail = this.bundle.getString("detail");
        textview_id.setText(rent_id);
        textview_detail.setText(detail);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        return view;
    }

    public interface OnDataListener{
        void onListener(Bundle bundle);
    }

    public void setOnDataListener(Fragment_Add_Booking.OnDataListener onDataListener){
        this.onDataListener = onDataListener;
    }

    public void setData(Bundle bundle){
        this.bundle = bundle;

    }
}
