package com.example.utmlifestyle_david.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.utmlifestyle_david.R;
import com.google.firebase.database.FirebaseDatabase;

public class Fragment_Date_Block extends Fragment {

    private Button btn;
    private OnButtonClick onButtonClick;

    private String id, name, date, time;

    public Fragment_Date_Block(String id, String name, String date, String time){
        this.id = id;
        this.name = name;
        this.date = date;
        this.time = time;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_block, container, false);
        btn = (Button)view.findViewById(R.id.button_block);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(name+";"+date+";"+l2s(time)).child("rent_status").setValue("b");
                if(onButtonClick!=null){
                    onButtonClick.onClick(btn);
                }
            }
        });
        return view;
    }



    public OnButtonClick getOnButtonClick() {
        return onButtonClick;
    }

    public void setOnButtonClick(OnButtonClick onButtonClick) {
        this.onButtonClick = onButtonClick;
    }

    public interface OnButtonClick{
        public void onClick(View view);
    }

    String l2s(String time){
        String[] sc = time.split("");
        String sn = "";
        for(int i = 0; i<2;i++){
            sn +=sc[i];
        }
        //Toast.makeText(TrackingMapActivity.this, sn, Toast.LENGTH_SHORT).show();
        return sn;
    }
}
