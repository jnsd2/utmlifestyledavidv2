package com.example.utmlifestyle_david.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.utmlifestyle_david.Model.Venue_List;
import com.example.utmlifestyle_david.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Fragment_Add_Venue extends Fragment {

    private DatabaseReference reference;
    private Context mContext;

    private Venue_List venue_list = new Venue_List();

    EditText editText_id, editText_name;
    private Button btn;
    private OnButtonClick onButtonClick;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_add_venue, container, false);
        btn = (Button)view.findViewById(R.id.button_add_venue_submit);
        editText_id = view.findViewById(R.id.edittext_id);
        editText_name = view.findViewById(R.id.edittext_name);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText_id.getText().toString().trim().isEmpty()||editText_name.getText().toString().trim().isEmpty()){
                    Toast.makeText(mContext, "Do not left only blank on id and name",Toast.LENGTH_LONG).show();
                }
                else{
                    reference = FirebaseDatabase.getInstance().getReference().child("Venue_List").child(editText_name.getText().toString());
                    venue_list.setId(editText_id.getText().toString());
                    venue_list.setName(editText_name.getText().toString());
                    reference.setValue(venue_list).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                //Toast.makeText(venue_booking_form.this, "Done", Toast.LENGTH_SHORT).show();
                                //startActivity(new Intent(Add_Venue.this, Add_Venue_Success_Activity.class));
                                if(onButtonClick!=null){
                                    onButtonClick.onClick(btn);
                                }
                            }
                        }
                    });
                }


            }
        });
        return view;
    }


    public OnButtonClick getOnButtonClick() {
        return onButtonClick;
    }

    public void setOnButtonClick(OnButtonClick onButtonClick) {
        this.onButtonClick = onButtonClick;
    }

    public interface OnButtonClick{
        public void onClick(View view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
