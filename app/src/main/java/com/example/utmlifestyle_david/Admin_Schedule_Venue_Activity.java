package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmlifestyle_david.Dialog.LoadDialogUtil;
import com.example.utmlifestyle_david.Model.Venue_booking_statuss;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Admin_Schedule_Venue_Activity extends AppCompatActivity {

    RecyclerView recyclerView_a, recyclerView_d;
    Button button_schedule_available_booking;

    LoadDialogUtil loadDialogUtil = new LoadDialogUtil();

    List<Venue_booking_statuss> booking_statussList_a = new ArrayList<>();
    List<Venue_booking_statuss> booking_statussList_d = new ArrayList<>();
    Booking_List_Adapter booking_list_adapter_a, booking_list_adapter_d;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_schedule_venue);

        loadDialogUtil.createLoadingDialog(Admin_Schedule_Venue_Activity.this, "Loading");

        recyclerView_a = findViewById(R.id.recyclerview_schedule_a_list);
        recyclerView_d = findViewById(R.id.recyclerview_schedule_d_list);

        recyclerView_a.setHasFixedSize(true);
        recyclerView_a.setLayoutManager(new LinearLayoutManager(Admin_Schedule_Venue_Activity.this));

        recyclerView_d.setHasFixedSize(true);
        recyclerView_d.setLayoutManager(new LinearLayoutManager(Admin_Schedule_Venue_Activity.this));

        button_schedule_available_booking = findViewById(R.id.button_schedule_available_booking);

        button_schedule_available_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Admin_Schedule_Venue_Activity.this, Schedule_Available_Booking_Activity.class);
                startActivity(intent);
            }
        });


        reference = FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                loadDialogUtil.closeDialog(loadDialogUtil.loadingDialog);
                booking_statussList_a.clear();
                booking_statussList_d.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()) {

                    Venue_booking_statuss venue_booking_statuss = snapshot1.getValue(Venue_booking_statuss.class);
                    if (venue_booking_statuss.getRent_status().equals("a")) {
                        booking_statussList_a.add(venue_booking_statuss);
                    }
                    else if(venue_booking_statuss.getRent_status().equals("b")){
                        booking_statussList_d.add(venue_booking_statuss);
                    }
                }
                booking_list_adapter_a = new Booking_List_Adapter(Admin_Schedule_Venue_Activity.this, booking_statussList_a);
                booking_list_adapter_a.setOnItemClickListener(new Booking_List_Adapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Bundle bundle = new Bundle();
                        bundle.putString("name", booking_statussList_a.get(position).getName());
                        bundle.putString("id", booking_statussList_a.get(position).getRent_id());
                        bundle.putString("time", booking_statussList_a.get(position).getTime());
                        bundle.putString("date", booking_statussList_a.get(position).getDate());
                        Intent intent = new Intent(Admin_Schedule_Venue_Activity.this, Block_Venue_Activity.class);
                        intent.putExtra("data", bundle);
                        startActivity(intent);
                    }
                });
                booking_list_adapter_d = new Booking_List_Adapter(Admin_Schedule_Venue_Activity.this, booking_statussList_d);
                booking_list_adapter_d.setOnItemClickListener(new Booking_List_Adapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Bundle bundle = new Bundle();
                        bundle.putString("name", booking_statussList_d.get(position).getName());
                        bundle.putString("id", booking_statussList_d.get(position).getRent_id());
                        bundle.putString("time", booking_statussList_d.get(position).getTime());
                        bundle.putString("date",booking_statussList_d.get(position).getDate());
                        Intent intent = new Intent(Admin_Schedule_Venue_Activity.this, Blockun_Venue_Activity.class);
                        intent.putExtra("data", bundle);
                        startActivity(intent);
                    }
                });
                recyclerView_a.setAdapter(booking_list_adapter_a);
                recyclerView_d.setAdapter(booking_list_adapter_d);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }
}