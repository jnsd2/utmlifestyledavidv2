package com.example.utmlifestyle_david;

import android.os.Bundle;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmlifestyle_david.Adapter.Bmi_History_Adapter;
import com.example.utmlifestyle_david.Model.BMI_HISTORY;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class BMIHistoryActivity extends AppCompatActivity {
    TextView textView;
    int height[] = new int[1];
    Button button;
    RecyclerView recyclerView;
    Bmi_History_Adapter adapter;
    List<BMI_HISTORY> bmi_historyArrayList = new ArrayList<>();
    DatabaseReference reference;
    LineChart mChart;
    ArrayList<Entry> y = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi_history);

        textView = findViewById(R.id.textView);
        button = findViewById(R.id.button_back);

        recyclerView = findViewById(R.id.recyclerview_bmi_history);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(BMIHistoryActivity.this));

        mChart = findViewById(R.id.linechart);
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(false);
        reference = FirebaseDatabase.getInstance().getReference("BMI");
        reference.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                bmi_historyArrayList.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()){
                    BMI_HISTORY bmi_history = snapshot1.getValue(BMI_HISTORY.class);
                    bmi_historyArrayList.add(bmi_history);
                }
                //Toast.makeText(BMIHistoryActivity.this,String.valueOf(bmi_historyArrayList.size()), Toast.LENGTH_SHORT).show();
                //Toast.makeText(TrackingListActivity.this,tracking_historyArrayList.get(0).getDistance(), Toast.LENGTH_SHORT).show();
                for(int i = 0; i < bmi_historyArrayList.size();i++){
                    y.add(new Entry(i,Float.parseFloat(bmi_historyArrayList.get(i).getBmi())));
                }
                String ww = "61.2";


                /*y.add(new Entry(0, Float.parseFloat(ww)));
                y.add(new Entry(1, 50f));
                y.add(new Entry(2, 70f));
                y.add(new Entry(3, 30f));
                y.add(new Entry(4, 60f));
                y.add(new Entry(5, 40f));
                y.add(new Entry(6, 50f));*/

                LineDataSet set = new LineDataSet(y, "BMI");

                set.setFillAlpha(110);

                ArrayList<ILineDataSet> dataset = new ArrayList();
                dataset.add(set);

                LineData data = new LineData(dataset);

                mChart.setData(data);
                mChart.invalidate();
                adapter = new Bmi_History_Adapter(BMIHistoryActivity.this, bmi_historyArrayList);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                height[0] = Math.round((button.getY()) - (mChart.getHeight() + mChart.getY()));
                recyclerView.getLayoutParams().height = height[0];
                //recyclerView.getLayoutParams().width= height[0];
                recyclerView.requestLayout();
                recyclerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        /*bmi_historyArrayList.add(new BMI_HISTORY("2020-09-02", "20", "170", "70"));
        bmi_historyArrayList.add(new BMI_HISTORY("2020-09-02", "20", "170", "70"));
        bmi_historyArrayList.add(new BMI_HISTORY("2020-09-02", "20", "170", "70"));
        bmi_historyArrayList.add(new BMI_HISTORY("2020-09-02", "20", "170", "70"));

        adapter = new Bmi_History_Adapter(BMIHistoryActivity.this, bmi_historyArrayList);
        recyclerView.setAdapter(adapter);*/
    }
}