package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.utmlifestyle_david.Fragment.Fragment_Add_Venue;
import com.example.utmlifestyle_david.Fragment.Fragment_Add_Venue_Success;
import com.example.utmlifestyle_david.Fragment.Fragment_Manage_Venue;
import com.example.utmlifestyle_david.Fragment.Fragment_Manage_Venue_Delete;
import com.example.utmlifestyle_david.Fragment.Fragment_Manage_Venue_Update;
import com.example.utmlifestyle_david.Model.Venue_List;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class Admin_Manage_Delete_Venue_Activity extends AppCompatActivity {

    TextView textView;
    EditText editText;
    FrameLayout frameLayout;
    ImageView imageView;

    Fragment_Manage_Venue fragment_manage_venue;
    Fragment_Manage_Venue_Update fragment_manage_venue_update = new Fragment_Manage_Venue_Update();
    Fragment_Manage_Venue_Delete fragment_manage_venue_delete = new Fragment_Manage_Venue_Delete();

    Fragment fragment = null;
    List<Fragment> fragmentList = new ArrayList<>();
    int i = 0;
    final int[] height = new int[1];

    WindowManager wm;
    Display displays;
    DisplayMetrics metrics;

    Button button_delete, button_update;



    String name, id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_manage_delete_venue);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        name = bundle.getString("name");
        id = bundle.getString("id");

        fragment_manage_venue = new Fragment_Manage_Venue(id, name);

        textView = findViewById(R.id.textview_venueid_delete);
        editText = findViewById(R.id.edittext_venue_name);
        button_update = findViewById(R.id.button_venue_update);
        button_delete = findViewById(R.id.button_venue_delete);





        wm = (WindowManager) getApplicationContext().getSystemService(Admin_Manage_Delete_Venue_Activity.this.WINDOW_SERVICE);
        displays = wm.getDefaultDisplay();
        metrics = new DisplayMetrics();
        displays.getMetrics(metrics);


        imageView = findViewById(R.id.imageView_top);
        frameLayout = findViewById(R.id.fragment_delete_venue);
        frameLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                height[0] = Math.round((metrics.heightPixels)-(imageView.getHeight()+imageView.getY()));
                frameLayout.getLayoutParams().height= height[0];
                frameLayout.requestLayout();
                frameLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        fragmentList.add(fragment_manage_venue);
        fragmentList.add(fragment_manage_venue_update);
        fragmentList.add(fragment_manage_venue_delete);
        fragment = fragmentList.get(i);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_delete_venue,fragment).commit();

        fragment_manage_venue.setOnButtonClick(new Fragment_Manage_Venue.OnButtonClick() {
            @Override
            public void onClick(View view) {

                            fragment = fragmentList.get(i+1);
                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_delete_venue,fragment).commit();



            }
        });

        fragment_manage_venue.setOnButtonClick2(new Fragment_Manage_Venue.OnButtonClick() {
            @Override
            public void onClick(View view) {

                        fragment = fragmentList.get(i+2);
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_delete_venue,fragment).commit();


            }
        });


    }
}