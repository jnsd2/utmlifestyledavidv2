package com.example.utmlifestyle_david.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmlifestyle_david.Model.Tracking_HISTORY;
import com.example.utmlifestyle_david.R;

import java.util.List;

public class Tracking_History_Adapter extends RecyclerView.Adapter<Tracking_History_Adapter.ViewHolder> {

    private Context context;
    private List<Tracking_HISTORY> tracking_historyList;
    private OnItemClickListener mOnItemClickListener;

    public Tracking_History_Adapter(Context context, List<Tracking_HISTORY> tracking_historyList){
        this.context = context;
        this.tracking_historyList = tracking_historyList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.tracking_history_card, parent, false);
        return new Tracking_History_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.date.setText(tracking_historyList.get(position).getDate()+", "+tracking_historyList.get(position).getStart_time()+" - "+tracking_historyList.get(position).getEnd_time());
        holder.timing.setText(tracking_historyList.get(position).getTiming());
        holder.detail.setText(tracking_historyList.get(position).getDistance()+" KM in "+tracking_historyList.get(position).getTime());
        if (mOnItemClickListener != null) {
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onItemClick(view, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return tracking_historyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView date, timing, detail;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.textview_tracking_date);
            timing = itemView.findViewById(R.id.textview_tracking_timing);
            detail = itemView.findViewById(R.id.textview_tracking_detail);
            cardView = itemView.findViewById(R.id.cardview);
        }
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener){
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
