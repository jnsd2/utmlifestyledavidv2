package com.example.utmlifestyle_david.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmlifestyle_david.Model.BMI_HISTORY;
import com.example.utmlifestyle_david.R;

import java.util.List;

public class Bmi_History_Adapter extends RecyclerView.Adapter<Bmi_History_Adapter.ViewHolder> {

    private Context context;
    private List<BMI_HISTORY> bmi_historyList;

    public Bmi_History_Adapter(Context context, List<BMI_HISTORY> bmi_historyList){
        this.context = context;
        this.bmi_historyList = bmi_historyList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.bmi_history_card, parent , false);
        return new Bmi_History_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.date.setText(bmi_historyList.get(position).getDate());
        holder.result.setText(bmi_historyList.get(position).getBmi());
        holder.height_weight.setText(bmi_historyList.get(position).getHeight()+"cm/"+bmi_historyList.get(position).getWeight()+"kg");
    }

    @Override
    public int getItemCount() {
        return bmi_historyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView date, result, height_weight;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.textview_bmi_date);
            result = itemView.findViewById(R.id.textview_bmi_result2);
            height_weight = itemView.findViewById(R.id.textview_bmi_height_weight);
        }
    }
}
