package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmlifestyle_david.Adapter.Tracking_History_Adapter;
import com.example.utmlifestyle_david.Model.Tracking_HISTORY;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TrackingListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Tracking_History_Adapter tracking_history_adapter;
    List<Tracking_HISTORY> tracking_historyArrayList = new ArrayList<>();
    Bundle bundle = new Bundle();
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_list);

        recyclerView = findViewById(R.id.recyclerview_tracking_history);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(TrackingListActivity.this));

        reference = FirebaseDatabase.getInstance().getReference("Map");
        reference.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                tracking_historyArrayList.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()){
                    Tracking_HISTORY tracking_history = snapshot1.getValue(Tracking_HISTORY.class);
                    tracking_historyArrayList.add(tracking_history);
                }
                //Toast.makeText(TrackingListActivity.this,String.valueOf(tracking_historyArrayList.size()), Toast.LENGTH_SHORT).show();
                //Toast.makeText(TrackingListActivity.this,tracking_historyArrayList.get(0).getDistance(), Toast.LENGTH_SHORT).show();
                tracking_history_adapter = new Tracking_History_Adapter(TrackingListActivity.this, tracking_historyArrayList);
                tracking_history_adapter.setOnItemClickListener(new Tracking_History_Adapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        bundle.putString("position", String.valueOf(position));
                        Intent intent = new Intent(TrackingListActivity.this, TrackingHistoryActivity.class);
                        intent.putExtra("data", bundle);
                        startActivity(intent);
                    }
                });
                recyclerView.setAdapter(tracking_history_adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}