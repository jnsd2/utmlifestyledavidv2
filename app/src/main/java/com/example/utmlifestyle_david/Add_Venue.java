package com.example.utmlifestyle_david;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.utmlifestyle_david.Fragment.Fragment_Add_Venue;
import com.example.utmlifestyle_david.Fragment.Fragment_Add_Venue_Success;
import com.example.utmlifestyle_david.Model.Venue_List;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class Add_Venue extends AppCompatActivity {


    FrameLayout frameLayout;
    ImageView imageView;


    Venue_List venue_list = new Venue_List();

    Fragment_Add_Venue fragment_add_venue = new Fragment_Add_Venue();
    Fragment_Add_Venue_Success fragment_add_venue_success = new Fragment_Add_Venue_Success();

    Fragment fragment = null;
    List<Fragment> fragmentList = new ArrayList<>();
    int i = 0;
    final int[] height = new int[1];

    WindowManager wm;
    Display displays;
    DisplayMetrics metrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_venue);
        wm = (WindowManager) getApplicationContext().getSystemService(Add_Venue.this.WINDOW_SERVICE);
        displays = wm.getDefaultDisplay();
        metrics = new DisplayMetrics();
        displays.getMetrics(metrics);


        frameLayout = findViewById(R.id.fragment);
        imageView = findViewById(R.id.imageView_top);



        frameLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                height[0] = Math.round((metrics.heightPixels)-(imageView.getHeight()+imageView.getY()));
                frameLayout.getLayoutParams().height= height[0];
                frameLayout.requestLayout();
                frameLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });


        fragmentList.add(fragment_add_venue);
        fragmentList.add(fragment_add_venue_success);
        fragment = fragmentList.get(i);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment,fragment).commit();

        fragment_add_venue.setOnButtonClick(new Fragment_Add_Venue.OnButtonClick() {
            @Override
            public void onClick(View view) {
                i++;
                fragment = fragmentList.get(i);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment,fragment).commit();
            }
        });

        /*button_add_venue_submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                reference = FirebaseDatabase.getInstance().getReference().child("Venue_List").child(editText_name.getText().toString());
                venue_list.setId(editText_id.getText().toString());
                venue_list.setName(editText_name.getText().toString());
                reference.setValue(venue_list).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            //Toast.makeText(venue_booking_form.this, "Done", Toast.LENGTH_SHORT).show();
                            //startActivity(new Intent(Add_Venue.this, Add_Venue_Success_Activity.class));
                            i++;
                            fragment = fragmentList.get(i);
                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment,fragment).commit();
                        }
                    }
                });
            }
        });*/
    }
}