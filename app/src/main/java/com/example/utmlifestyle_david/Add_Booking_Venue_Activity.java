package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.utmlifestyle_david.Dialog.LoadDialogUtil;
import com.example.utmlifestyle_david.Fragment.Fragment_Add_Booking;
import com.example.utmlifestyle_david.Fragment.Fragment_Add_Booking_Success;
import com.example.utmlifestyle_david.Fragment.Fragment_Add_Venue;
import com.example.utmlifestyle_david.Fragment.Fragment_Add_Venue_Success;
import com.example.utmlifestyle_david.Model.Venue_booking_statuss;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Add_Booking_Venue_Activity extends AppCompatActivity {

    Spinner spinner_sport_hall, spinner_date, spinner_time;
    Button button_confirm;
    FrameLayout frameLayout;
    ImageView imageView;

    Fragment_Add_Booking fragment_add_booking = new Fragment_Add_Booking();
    Fragment_Add_Booking_Success fragment_add_booking_success = new Fragment_Add_Booking_Success();

    Fragment fragment = null;
    List<Fragment> fragmentList = new ArrayList<>();
    int j = 0;
    final int[] height = new int[1];

    WindowManager wm;
    Display displays;
    DisplayMetrics metrics;


    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_booking_venue);

        wm = (WindowManager) getApplicationContext().getSystemService(Add_Booking_Venue_Activity.this.WINDOW_SERVICE);
        displays = wm.getDefaultDisplay();
        metrics = new DisplayMetrics();
        displays.getMetrics(metrics);

        frameLayout = findViewById(R.id.fragment);
        imageView = findViewById(R.id.imageView_top);



        frameLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                height[0] = Math.round((metrics.heightPixels)-(imageView.getHeight()+imageView.getY()));
                frameLayout.getLayoutParams().height= height[0];
                frameLayout.requestLayout();
                frameLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });


        fragmentList.add(fragment_add_booking);
        fragmentList.add(fragment_add_booking_success);
        fragment = fragmentList.get(i);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment,fragment).commit();

        fragment_add_booking.setOnButtonClick(new Fragment_Add_Booking.OnButtonClick() {
            @Override
            public void onClick(View view) {
                j++;
                fragment = fragmentList.get(j);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment,fragment).commit();
            }
        });

        fragment_add_booking.setOnDataListener(new Fragment_Add_Booking.OnDataListener() {
            @Override
            public void onListener(Bundle bundle) {
                fragment_add_booking_success.setData(bundle);
            }
        });

        /*loadDialogUtil.createLoadingDialog(Add_Booking_Venue_Activity.this, "Loading");
        spinner_sport_hall = findViewById(R.id.spinner_sport_hall);
        spinner_date = findViewById(R.id.spinner_date);
        spinner_time = findViewById(R.id.spinner_time);
        button_confirm = findViewById(R.id.button_confirm);

        reference = FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                loadDialogUtil.closeDialog(loadDialogUtil.loadingDialog);
                sport_hall.clear();
                venue_booking_statusList.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()) {
                    Venue_booking_statuss venue_booking_status1 = snapshot1.getValue(Venue_booking_statuss.class);
                    venue_booking_statusList.add(venue_booking_status1);

                    if(sport_hall.size()==0){
                        sport_hall.add(venue_booking_status1.getName());
                    }
                    else{
                        for(int i = 0; i<sport_hall.size();i++){
                            if(venue_booking_statusList.get(i).getName().equals(venue_booking_status1.getName())){

                            }
                            else{
                                sport_hall.add(venue_booking_status1.getName());
                            }
                        }
                    }
                }

                adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, sport_hall);
                adapter.setDropDownViewResource(R.layout.spinner_text);
                spinner_sport_hall.setAdapter(adapter);
                spinner_sport_hall.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        text_spinnerplace = parent.getItemAtPosition(position).toString();
                        avaiable_date.clear();
                        for(int i = 0 ; i<venue_booking_statusList.size() ; i++){
                            if(avaiable_date.size()==0){
                                avaiable_date.add(venue_booking_statusList.get(i).getDate());
                            }
                            else{

                                if(venue_booking_statusList.get(i).getDate().equals(avaiable_date.get(avaiable_date.size()-1))){

                                }
                                else{
                                    avaiable_date.add(venue_booking_statusList.get(i).getDate());
                                }
                            }
                        }
                        adapterdate = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, avaiable_date);
                        adapterdate.setDropDownViewResource(R.layout.spinner_text);
                        spinner_date.setAdapter(adapterdate);
                        spinner_date.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                text_spinnerdate = parent.getItemAtPosition(position).toString();
                                //Toast.makeText(parent.getContext(),text,Toast.LENGTH_SHORT).show();
                                avaiable_time.clear();
                                for(int i = 0 ; i<venue_booking_statusList.size() ; i++){
                                    if(venue_booking_statusList.get(i).getName().equals(text_spinnerplace)&&venue_booking_statusList.get(i).getDate().equals(text_spinnerdate)&&venue_booking_statusList.get(i).getRent_status().equals("a")){
                                        avaiable_time.add(venue_booking_statusList.get(i).getTime());
                                    }
                                }

                                adaptertime = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_text, avaiable_time);
                                adaptertime.setDropDownViewResource(R.layout.spinner_text);
                                spinner_time.setAdapter(adaptertime);
                                spinner_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        text_spinnertime = parent.getItemAtPosition(position).toString();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        button_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] timesplit = text_spinnertime.split("");
                String timeshort = timesplit[0]+timesplit[1];
                String child = text_spinnerplace+";"+text_spinnerdate+";"+timeshort;
                //Toast.makeText(venue_booking_form.this,child,Toast.LENGTH_SHORT).show();
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(child).child("pay_method").setValue("Chat");
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(child).child("rent_id").setValue("A-123CH");
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(child).child("rent_person_name").setValue("david");
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statusss").child(child).child("rent_status").setValue("n");

                String detail = "";
                for(int i = 0 ; i < venue_booking_statusList.size(); i++){
                    if(venue_booking_statusList.get(i).getName().equals(text_spinnerplace)&&venue_booking_statusList.get(i).getDate().equals(text_spinnerdate)&&venue_booking_statusList.get(i).getTime().equals(text_spinnertime)){
                        detail = venue_booking_statusList.get(i).getVenue_detail();
                        break;
                    }
                }

                Bundle bundle = new Bundle();
                bundle.putString("rent_id", "A-123CH");
                bundle.putString("detail",detail);
                Intent i = new Intent(Add_Booking_Venue_Activity.this,Add_Booking_Success_Activity.class);
                i.putExtra("bundle",bundle);
                startActivity(i);
            }
        });*/

    }
}