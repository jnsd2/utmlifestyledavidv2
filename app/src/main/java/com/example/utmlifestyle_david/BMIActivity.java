package com.example.utmlifestyle_david;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

public class BMIActivity extends AppCompatActivity {
    Button image;
    LinearLayout linearLayout, linearLayout2;
    EditText editText_height, editText_weigth;
    ImageView imageview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);

        image = findViewById(R.id.buttoncalculate);
        linearLayout = findViewById(R.id.linearLayout3);
        linearLayout2 = findViewById(R.id.linearLayout2);
        imageview = findViewById(R.id.imageView2);

        editText_height = findViewById(R.id.edittextheight);
        editText_weigth = findViewById(R.id.edittextweight);


        final int[] height = new int[1];
        final String sy = String.valueOf(linearLayout.getPivotY());
        final float y = linearLayout.getPivotY();

        final Bundle bundle = new Bundle();

        imageview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                height[0] = Math.round((linearLayout.getY())-(linearLayout2.getHeight()+linearLayout2.getY()));
                imageview.getLayoutParams().height= height[0];
                imageview.getLayoutParams().width= height[0];
                imageview.requestLayout();
                imageview.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bundle.putString("height", editText_height.getText().toString());
                bundle.putString("weight", editText_weigth.getText().toString());
                Intent intent = new Intent(BMIActivity.this, BMIResultActivity.class);
                intent.putExtra("data", bundle);
                startActivity(intent);
                //Toast.makeText(MainActivity.this, "("+linearLayout.getPivotY()+"+"+linearLayout.getY()+")"+"-"+"("+image.getPivotY()+"+"+image.getY()+")"+"="+height2, Toast.LENGTH_LONG).show();
            }
        });

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "linear"+ height[0], Toast.LENGTH_LONG).show();
                Intent intent = new Intent(BMIActivity.this, BMIHistoryActivity.class);
                startActivity(intent);
            }
        });

        editText_weigth.addTextChangedListener(textWatcher);
        editText_height.addTextChangedListener(textWatcher);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String height = editText_height.getText().toString().trim();
            String weight = editText_weigth.getText().toString().trim();

            image.setEnabled(!height.isEmpty() && !weight.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
}